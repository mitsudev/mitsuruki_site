FROM python:3.10.2

RUN pip install pip --upgrade \
    && pip install poetry

COPY backend /app/backend
COPY frontend /app/frontend

WORKDIR /app/backend

RUN poetry install
