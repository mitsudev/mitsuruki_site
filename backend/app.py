from flask import Flask

#from auth import auth
from api import blueprint as api

from config import Config

from models import db, migrate

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)

    app.register_blueprint(api, url_prefix='/api/')

    return app

if __name__ == '__main__':
    app = create_app()
