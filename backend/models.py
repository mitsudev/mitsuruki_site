from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy.sql import func

db = SQLAlchemy()
migrate = Migrate()

class Activity(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100)) 
    intro = db.Column(db.String(300)) 
    content = db.Column(db.Text)
    author_id = db.Column(db.Integer) #id_user сreator Activity
    comments_is_on = db.Column(db.Boolean, default=False) 
    activity_type = db.Column(db.Text) # news, events
    activity_date = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime,server_default=func.now())

    def __repr__(self):
        return 'Activity %r' % self.id


class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100)) 
    intro = db.Column(db.String(300)) 
    content = db.Column(db.Text)
    author_id = db.Column(db.Integer) #id_user сreator Article
    comments_is_on = db.Column(db.Boolean, default=False) 
    category = db.Column(db.Text) # review  
    created_at = db.Column(db.DateTime,server_default=func.now())

    def __repr__(self):
        return 'Article %r' % self.id


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(64)) #унікальний 
    password = db.Column(db.String(64)) 
    name = db.Column(db.String(100)) 
    status = db.Column(db.String(100)) #'dev' 'admin' 'moderator' 'user' 'cutedUser' 'muted' 'baned'
    filia_id = db.Column(db.Integer)

    def __repr__(self):
        return 'User %r' % self.id


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    foreign_id = db.Column(db.Integer)
    type_id = db.Column(db.Integer)
    message = db.Column(db.Text)
    created_at = db.Column(db.DateTime,server_default=func.now())

    
    def __repr__(self):
        return 'Comment %r' % self.id
