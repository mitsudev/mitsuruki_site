from flask import Blueprint

auth = Blueprint("auth", __name__)

@auth.route("/auth/login")
def login():
    return {"login":"ok"}

@auth.route("/auth/register")
def register():
    return {"register":"ok"}
