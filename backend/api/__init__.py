from flask import Blueprint
from flask_restx import Api

from .activity import api as activity
from .article import api as article
from .users import api as users


blueprint = Blueprint('api', __name__)

api = Api(
    blueprint,
    title='Mitsuruki Api',
    description='Api для сайту клубу',
)

api.add_namespace(activity)
api.add_namespace(article)
api.add_namespace(users)