from flask_restx import Namespace, Resource, fields
from models import Article 


api = Namespace('Article',path='/article', description='Статті сайту')

schema = api.model('Article', {
    'id': fields.Integer(readonly=True),
    'title': fields.String,
    'intro': fields.String,
    'content': fields.String,
    'author_id': fields.Integer,
    'comments_is_on': fields.Boolean,
    'category': fields.String,
    'created_at': fields.DateTime(readonly=True)
})


@api.route('/')
class ArticleListResource(Resource):
    @api.marshal_list_with(schema)
    def get(self):
        return Article.query.all()

    @api.expect(schema)
    def post(self):
        article=Article(**api.payload)
        db.session.add(article)
        db.session.commit()
        return {'id':article.id}


@api.route('/<int:id>')
class ArticleResource(Resource):
    @api.marshal_with(schema)
    def get(self, id):
        return Article.query.get_or_404(id)

    @api.expect(schema)
    def put(self, id):
        article=Article(id=id,**api.payload)
        db.session.merge(article)
        db.session.commit()
        return {'id':article.id}

    def delete(self, id):
        article = Article.query.get_or_404(id)
        db.session.delete(article)
        db.session.commit()
        return {'id':article.id}

