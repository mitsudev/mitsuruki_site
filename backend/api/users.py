from flask_restx import Namespace, Resource, fields
from models import db, User


api = Namespace('Users',path='/users', description='Користувачі')

schema = api.model('User', {
    'id': fields.Integer(readonly=True),
    'login': fields.String,
    'password': fields.String,
    'name': fields.String,
    'status': fields.String,
    'filia_id': fields.Integer,
})


@api.route('/<int:id>')
class UserResource(Resource):
    @api.marshal_with(schema,skip_none=True)
    def get(self, id):
        user = User.query.get_or_404(id)
        user.password=None
        return user

    @api.expect(schema)
    def post(self):
        user=User(**api.payload)
        db.session.add(user)
        db.session.commit()
        return {'id':user.id}

    @api.expect(schema)
    def put(self, id):
        user=User(id=id,**api.payload)
        db.session.merge(user)
        db.session.commit()
        return {'id':user.id}
