from flask_restx import Namespace, Resource, fields
from models import db, Activity


api = Namespace('Activity',path='/activity', description='Діяльність сайту')

schema = api.model('Activity', {
    'id': fields.Integer(readonly=True),
    'title': fields.String,
    'intro': fields.String,
    'content': fields.String,
    'author_id': fields.Integer,
    'comments_is_on': fields.Boolean,
    'activity_type': fields.Integer,
    'activity_date':fields.DateTime,
    'created_at': fields.DateTime(readonly=True)
})




@api.route('/')
class ActivityListResource(Resource):
    @api.marshal_list_with(schema)
    def get(self):
        return Activity.query.all()

    @api.expect(schema)
    def post(self):
        activity=Activity(**api.payload)
        db.session.add(activity)
        db.session.commit()
        return {'id':activity.id}


@api.route('/<int:id>')
class ActivityResource(Resource):
    @api.marshal_with(schema)
    def get(self, id):
        return Activity.query.get_or_404(id)

    @api.expect(schema)
    def put(self, id):
        activity=Activity(id=id,**api.payload)
        db.session.merge(activity)
        db.session.commit()
        return {'id':activity.id}

    def delete(self, id):
        activity = Activity.query.get_or_404(id)
        db.session.delete(activity)
        db.session.commit()
        return {'id':activity.id}