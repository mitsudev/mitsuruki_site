export const get = async <T>(url: string): Promise<T> => {
  const response = await fetch(url);
  const payload = await response.json();

  if (response.status !== 200) {
    throw payload;
  }

  return payload;
};
