import { Typography } from "@mui/material";
import NotFound from "components/NotFound";
import { useState, useEffect } from "react";
import ReactMarkdown from "react-markdown";
import { useParams } from "react-router-dom";
import { getNewsPiece } from "services/news";
import { NewsCardProps } from "types";

const NewsPiecePage = () => {
  const urlParams = useParams();

  const id = Number(urlParams.id);

  if (!id) {
    return <NotFound />;
  }

  const [news, setNews] = useState<NewsCardProps | undefined>(undefined);
  const [error, setError] = useState<string | undefined>(undefined);

  useEffect(() => {
    if (!news) {
      getNewsPiece(id).then(setNews).catch(setError);
    }
  }, [setNews]);

  if (error) {
    return <NotFound />;
  }

  if (!news) {
    return <h2>Loading...</h2>;
  }

  return (
    <>
      <h1>{news.title}</h1>
      <Typography>
        <ReactMarkdown>{news.content}</ReactMarkdown>
      </Typography>
    </>
  );
};

export default NewsPiecePage;
