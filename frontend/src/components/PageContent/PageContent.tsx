import {
  BrowserRouter,
  Outlet,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
import Header from "components/Header";
import MainPage from "components/MainPage";
import News from "components/News";
import NewsPiecePage from "components/NewsPiece";
import NotFound from "components/NotFound";
import "styles/page.scss";
import "styles/pageContentTable.scss";
import FloatingActionMenu from "components/FloatingActionMenu";
import { ReactElement, useEffect } from "react";

// const Articles = () => {
//   return <h2>Статті</h2>;
// };

// const Activities = () => {
//   return <h2>Діяльність</h2>;
// };
// const Projects = () => {
//   return <h2>Проекти</h2>;
// };
// const Filias = () => {
//   return <h2>Осередки</h2>;
// };

const withHashLinkScroll = (component: ReactElement) => {
  const { pathname, hash, key } = useLocation();

  useEffect(() => {
    // if not a hash link, scroll to top
    if (hash === "") {
      window.scrollTo(0, 0);
    }
    // else scroll to id
    else {
      setTimeout(() => {
        const rootId = "root";
        const id = hash.replace("#", "");
        const element =
          document.getElementById(id) || document.getElementById(rootId);

        if (element) {
          element.scrollIntoView({
            behavior: "smooth",
          });
        }
      }, 0);
    }
  }, [pathname, hash, key]); // do this on route change

  return component;
};

const AppLayout = () => {
  const layout = (
    <div className="page">
      <div className="page-content-table">
        <div className="page-content-table-top">
          <div />
          <div />
          <div />
          <div />
          <div />
        </div>
        <div className="page-content-table-middle">
          <div className="page-content-table-middle-left" />
          <div className="page-content-table-middle-middle page-content">
            <Outlet />
          </div>
          <div className="page-content-table-middle-right" />
        </div>
        <div className="page-content-table-bottom">
          <div />
          <div />
          <div />
        </div>
      </div>
    </div>
  );

  return withHashLinkScroll(layout);
};

const PageContent = () => {
  return (
    <BrowserRouter>
      <Header />
      <FloatingActionMenu />

      <Routes>
        <Route element={<AppLayout />}>
          <Route path="/" element={<MainPage />} />
          <Route path="/news" element={<News />} />
          <Route path="/news/:id" element={<NewsPiecePage />} />
          {/* <Route path="/activities" element={<Activities />} />
          <Route path="/articles" element={<Articles />} />
          <Route path="/projects" element={<Projects />} />
          <Route path="/filias" element={<Filias />} /> */}
          <Route path="/*" element={<NotFound />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default PageContent;
