import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { TopMenuItem, TopMenuProps } from "types";

const createItem = (item: TopMenuItem, i: number) => {
  const navigate = useNavigate();

  return (
    <Button key={i} onClick={() => navigate(item.url)}>
      {item.label}
    </Button>
  );
};

const TopMenu = ({ items }: TopMenuProps) => {
  return <div>{items.map(createItem)}</div>;
};

export default TopMenu;
