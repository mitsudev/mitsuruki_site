import { Stack, Card, CardMedia, Typography } from "@mui/material";
import { useState, useEffect } from "react";
import ReactMarkdown from "react-markdown";
import { getNews } from "services/news";
import { NewsCardProps } from "types";

const News = () => {
  const [news, setNews] = useState<NewsCardProps[]>([]);

  useEffect(() => {
    if (!news.length) {
      getNews().then(setNews);
    }
  }, [setNews]);

  return (
    <Stack spacing={2} className="news-container">
      {news.map((nc, i) => (
        <Card key={i} className="news-card">
          <CardMedia
            className="news-card-media"
            image={nc.image}
            title={nc.title}
          >
            <Typography>
              <a href={`/news/${nc.id}`}>{nc.title}</a>
            </Typography>
            <Typography>
              <ReactMarkdown>{nc.content}</ReactMarkdown>
            </Typography>
          </CardMedia>
        </Card>
      ))}
    </Stack>
  );
};

export default News;
