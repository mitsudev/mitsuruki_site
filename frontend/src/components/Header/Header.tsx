import { Button } from "@mui/material";
import Logo from "assets/logoHeader.svg";
import TopMenu from "components/TopMenu";
import { useNavigate } from "react-router-dom";
import "styles/topBar.scss";

const Header = () => {
  const navigate = useNavigate();

  const topMenuItems = [
    {
      label: "Діяльність",
      url: "#activities",
    },
    {
      label: "Проекти",
      url: "#projects",
    },
    {
      label: "Осередки",
      url: "#filias",
    },
  ];

  return (
    <div className="top-bar">
      <div className="top-bar-content">
        <Button onClick={() => navigate("/")}>
          <img className="logo" src={Logo} alt="logo" />
          <div className="site-name">MITSURUKI</div>
        </Button>
        <TopMenu items={topMenuItems} />
      </div>
    </div>
  );
};

export default Header;
