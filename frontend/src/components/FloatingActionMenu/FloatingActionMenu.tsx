import { Fab, SvgIcon } from "@mui/material";
import { ReactComponent as floatingActionButtonLogo } from "assets/floatingActionButtonLogo.svg";
import floatingRopeBrush from "assets/floatingRopeBrush.svg";
import "styles/fab.scss";

const FloatingActionMenu = () => {
  return (
    <>
      <div className="fab-rope" />
      <img className="fab-rope-brush" src={floatingRopeBrush} />

      <Fab className="fab-button">
        <SvgIcon component={floatingActionButtonLogo} inheritViewBox />
      </Fab>
    </>
  );
};

export default FloatingActionMenu;
