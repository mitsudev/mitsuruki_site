import { NewsCardProps } from "types";
import { get } from "utils/client";

export const getNews = () => get<NewsCardProps[]>("/api/news/");
export const getNewsPiece = (id: number) =>
  get<NewsCardProps>(`/api/news/${id}`);
