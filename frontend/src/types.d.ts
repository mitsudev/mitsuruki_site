export type NewsCardProps = {
  id: number;
  title: string;
  content: string;
  image?: string;
};

export type SliderItem = {
  name: string;
  caption: string;
  image: string;
};
export type SliderProps = {
  items: SliderItem[];
};

export type TopMenuItem = {
  label: string;
  url: string;
  children?: TopMenuItem[];
};
export type TopMenuProps = {
  items: TopMenuItem[];
};
